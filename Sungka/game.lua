
local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

local physics = require "physics"
physics.start(); physics.pause()

local widget = require( "widget" )

local screenW, screenH, halfW, halfH = display.contentWidth, display.contentHeight, display.contentWidth*0.5, display.contentHeight*0.5

local numPlayers = 1
local realPath = {}
local numBahay = 7
local numShells = 7
local messageA
local messageB
local autoSwitch
local isSimultaneousPlay = false
local curPlayer = ""
local isQuitting = false

local noFill = {1, 1, 1}
local colorA = "green"
local fillA = {0, 1, 0}
local colorB = "blue"
local fillB = {0, 1, 1}
local fillBoth = {0, 0, 1}
local CPUPlayer = "B"
local dropSound = audio.loadSound("shellDrop.wav")
local pickupSound = audio.loadSound("shellPickup.wav")
local errorSound = audio.loadSound("error.wav")

local CPU_Easy 		= 0 -- pick hole with most shells
local CPU_Medium 	= 1 -- pick best score of 2 pickups per hole
local CPU_Hard 		= 2 -- pick best score of 5 pickups per hole
local CPU_Master 	= 3 -- do well known techniques then do CPU_Hard
local CPULevel = CPU_Hard

local distributeShellsTimer = 500

local function sendMessage(player, msg)
	if player == "A" then
		messageA.text = msg
	else
		messageB.text = msg
	end
end

local function incrementPathNdx(path, pathNdx)
	return pathNdx % #path + 1
end

local function getOtherPlayer(player)
	if player == "A" then
		return "B"
	else
		return "A"
	end
end

local function getHoleAcrossFrom(hole, path)
	local id = getOtherPlayer(hole.id:sub(1,1)) .. 
		numBahay + 1 - tonumber(hole.id:sub(2,2))
	return path.lookup[id]
end

local function getHomeHole(player, path)
	return path.lookup[player .. "0"]
end

local function isCPUPlayer(player)
	return numPlayers == 1 and player == CPUPlayer
end

local function getColor(player)
	if player == "A" then
		return colorA
	else
		return colorB
	end
end

local function isQuittingNow() 
	return isQuitting
end

local function setupNextRoundFor(player, path)
	print("setting up round for " .. player)
	local playerShells = getHomeHole(player, path).numShells
	local sunogNa = false
	for i = 1, numBahay, 1 do
		local hole = path.lookup[player .. i]
		hole:setSunog(false)
		if sunogNa then
			hole:setSunog(true)
			hole:setNumShells(0)
		elseif playerShells <= numShells then
			hole:setNumShells(playerShells)
			playerShells = 0
			sunogNa = true
		else
			hole:setNumShells(numShells)
			playerShells = playerShells - numShells
		end
	end
	getHomeHole(player, path):setNumShells(playerShells)
end

local function setupNextRound(path)
	setupNextRoundFor("A", path)
	setupNextRoundFor("B", path)
end

local switchTurns
local distributeShellsAt
local highlightAvailableHoles
local clearHighlightsFor

local function copyHole(hole)
	local copy = {}
	copy.id = hole.id
	copy.pathNdx = hole.pathNdx
	copy.numShells = hole.numShells
	copy.isSunog = hole.isSunog
	copy.setNumShells = function (self, count)
		self.numShells = count
	end
	copy.highlight = function (self) end
	copy.unHighlight = function (self) end
	return copy
end

local function copyPath(path)
	local copy = {}
	for i = 1, #path, 1 do
		copy[i] = copyHole(path[i])
	end

	local copyLookup = {}
	for i=1, #copy, 1 do
		local hole = copy[i]
		copyLookup[hole.id] = hole
	end
	copy.lookup = copyLookup
	return copy
end

local function scoreMove(player, path, hole, maxPickups)
	print("scoring " .. hole.id .. " with maxPickups of " .. maxPickups)
	local copiedPath = copyPath(path)
	local score = distributeShellsAt(copiedPath, copiedPath.lookup[hole.id], player, maxPickups)
	print("  " .. hole.id .. " scored " .. score)
	return score
end

local function scoreHole(player, path, hole)
	if CPULevel == CPU_Medium then
		return scoreMove(player, path, hole, 3)
	elseif CPULevel == CPU_Hard then 
		return scoreMove(player, path, hole, 8)
	else
		return hole.numShells
	end
end

local function takeCPUTurn(path)
	sendMessage(CPUPlayer, "thinking...")
	print("CPU thinking...")
	local options = {}

	-- figure out possible moves
	for i = 1, #path, 1 do
		local hole = path[i]
		if hole.id:sub(1,1) == CPUPlayer and hole.isHighlighted then
			options[#options + 1] = hole
		end
	end

	-- find best scoring hole
	local bestScore = -1
	local bestHole = nil
	for i = 1, #options, 1 do
		local hole = options[i]
		local thisHoleScore = scoreHole(CPUPlayer, path, hole)
		if thisHoleScore > bestScore then
			bestHole = hole
			bestScore = thisHoleScore
		end
	end

	if bestHole == nil then
		print("ERROR: CPU couldn't find a move.")
	else
		clearHighlightsFor(path, CPUPlayer)
		print("CPU picking up shells at " .. bestHole.id)
		distributeShellsAt(path, bestHole, CPUPlayer)
	end
end

local function continueTurnFor(path, player)
	if highlightAvailableHoles(path, player) then 
		if isCPUPlayer(player) then
			takeCPUTurn(path)
		else
			sendMessage(player, "Touch a " .. getColor(player) .. " hole to move.")
		end
	end
end

-- local forward declaration somewhere above
function switchTurns(path)
	curPlayer = getOtherPlayer(curPlayer)
	print("switching to player " .. curPlayer)
	continueTurnFor(path, curPlayer)
end

local function simultaneousTurns(path)
	isSimultaneousPlay = true
	continueTurnFor(path, "A")
	continueTurnFor(path, "B")
end

local function endRound(path, winner)
	for i = 1, #path, 1 do
		local hole = path[i]
		if hole.id:sub(2,2) ~= "0" then
			local thisPlayer = hole.id:sub(1,1)
			getHomeHole(thisPlayer, path):setNumShells(getHomeHole(thisPlayer, path).numShells + hole.numShells)
		end
	end
	print(winner .. " wins with " .. getHomeHole(winner, path).numShells .. " shells" )
	print(getOtherPlayer(winner) .. " loses with " .. getHomeHole(getOtherPlayer(winner), path).numShells .. " shells" )

	local isEndGame = getHomeHole("A", path).numShells < numShells or getHomeHole("B", path).numShells < numShells
	if isEndGame then
		if getHomeHole("A", path).numShells > getHomeHole("B", path).numShells then
			winner = "A"
		else
			winner = "B"
		end
		print("Player " .. winner .. " wins!")
		sendMessage(winner, "You win! Congratulations!")
		sendMessage(getOtherPlayer(winner), "You lose!")
	else
		print("Player " .. winner .. " wins this round.")
		if not isCPUPlayer(winner) then 
			sendMessage(winner, "You win this round.")
		end

		if not isCPUPlayer(getOtherPlayer(winner)) then 
			sendMessage(getOtherPlayer(winner), "You lose this round.")
		end
		setupNextRound(path)
		continueTurnFor(path, winner)
	end
end

-- local forward declaration somewhere above
function highlightAvailableHoles(path, player)
	print("highlightAvailableHoles " .. player)
	local availCount = 0
	for i = 1, #path, 1 do
		local hole = path[i]
		if hole.id:sub(1, 1) == player and hole.id:sub(2, 2) ~= "0" and hole.numShells > 0 then
			hole:highlight(player)
			availCount = availCount + 1
		end
	end
	if availCount == 0 then
		print("no holes available for " .. player)
		endRound(path, getOtherPlayer(player))
		return false
	end
	return true
end

local function clearHighlights(path)
	print("clearHighlights")
	for i = 1, #path, 1 do
		local hole = path[i]
		hole:unHighlight()
	end
end

-- local forward declaration somewhere above
function clearHighlightsFor(path, player)
	print("clearHighlightsFor " .. player)
	for i = 1, #path, 1 do
		local hole = path[i]
		if hole.id:sub(1,1) == player then
			hole:unHighlight(player)
		end
	end
end

function doStep(delay, step)
	if delay == 0 then
		step()
	else
		timer.performWithDelay(delay, step)
	end
end

-- local forward declaration somewhere above
function distributeShellsAt(path, hole, player, maxPickups)
	local score = 0
	local isSimulatingMove = maxPickups ~= nil
	local timerLength = distributeShellsTimer
	if isSimulatingMove then
		timerLength = 0
	elseif isCPUPlayer(player) then
		timerLength = timerLength / 2
	end

	if maxPickups == 0 then
		return 0
	end

	sendMessage(player, "moving...")
	print(player .. " -> distributing " .. hole.numShells .. " shells from " .. hole.id .. " with maxPickups " .. (maxPickups ~= nil and maxPickups or "0") )

	if maxPickups ~= nil then
		maxPickups = maxPickups - 1
	end

	if not isSimulatingMove then
		audio.play(pickupSound)
	end
	local numShells = hole.numShells

	hole:highlight(player)
	hole:setNumShells(0)

	local pathNdx = hole.pathNdx
	for i = 1, numShells, 1 do
		if isQuittingNow() then
			return
		end

		local prevHole = path[pathNdx]

		pathNdx = incrementPathNdx(path, pathNdx)
		local nextHole = path[pathNdx]
		while nextHole.isSunog or 
			(nextHole.id:sub(2,2) == "0" and nextHole.id ~= getHomeHole(player, path).id) do -- skip opponent main hole
			pathNdx = incrementPathNdx(path, pathNdx)
			nextHole = path[pathNdx]
		end
		local nextStep = function() 
			if isQuittingNow() then
				return
			end
			prevHole:unHighlight(player)
			nextHole:highlight(player)
			if not isSimulatingMove then
				audio.play(dropSound) 
			end

			nextHole:setNumShells(nextHole.numShells + 1)
		end
		doStep(timerLength*i, nextStep)
	end
	local endHole = path[pathNdx]
	local endStep = function()
		endHole:unHighlight(player)
		if endHole.id == getHomeHole(player, path).id then
			sendMessage(player, "Subi! You get another turn.")
			if not isSimulatingMove then
				doStep(timerLength*2, function() continueTurnFor(path, player) end)
			end
			print(player .. " gets another turn.")
			score = score + 10
		elseif endHole.numShells == 1 then
			print(player .. " -> ending turn at " .. endHole.id)
			if endHole.id:sub(1,1) == player then
				local holeAcross = getHoleAcrossFrom(endHole, path)
				score = score + 1 + holeAcross.numShells
				print(player .. " -> getting " .. holeAcross.numShells .. " shells from " .. holeAcross.id)
				getHomeHole(player, path):setNumShells(getHomeHole(player, path).numShells + holeAcross.numShells + endHole.numShells)
				holeAcross:setNumShells(0)
				endHole:setNumShells(0)
			end
			if not isSimulatingMove then
				if isSimultaneousPlay then
					isSimultaneousPlay = false
				else
					switchTurns(path)
				end
			end
		else
			if autoSwitch.isOn or isCPUPlayer(player) or isSimulatingMove then
				--print(player .. " -> picking up " .. endHole.numShells .. " shells from " .. endHole.id)
				score = score + 1 + distributeShellsAt(path, endHole, player, maxPickups)
			else
				print(player .. " -> waiting for pick up of " .. endHole.numShells .. " shells at " .. endHole.id)
				endHole:highlight(player)
				sendMessage(player, "Touch the " .. getColor(player) .. " hole to continue moving.")
			end
		end
	end
	sendMessage(player, "")
	doStep(timerLength*(numShells+1), endStep)

	return score
end

local function onHoleTouch(self, event)
	if event.numTaps == 1 then
		if not self.isHighlighted then
			audio.play(errorSound)
		else
			local thisPlayer = self.id:sub(1,1)
			if isSimultaneousPlay then
				clearHighlightsFor(realPath, thisPlayer)
				distributeShellsAt(realPath, self, thisPlayer)
			else
				clearHighlights(realPath)
				distributeShellsAt(realPath, self, thisPlayer)
			end
		end
	end
end

local function makeHole(id, group, pathNdx, x, y, width, height, numShells)
	local hole = {}
	hole.id = id
	hole.pathNdx = pathNdx
	
	local circle = display.newRoundedRect( x, y + height/2, width, height, 3 ) 
	hole.circle = circle
	hole.circle:addEventListener( "tap", hole )
	group:insert( hole.circle )
	
	hole.setupBodies = function (self)
		self.top = display.newRect(x, y, width, 1)
		group:insert( self.top )
		physics.addBody( self.top, "static", { friction=0.0 } )

		self.bottom = display.newRect(x, y + height, width, 1)
		group:insert( self.bottom )
		physics.addBody( self.bottom, "static", { friction=0.0 } )

		self.left = display.newRect(circle.x - (width/2), circle.y, 1, height )
		group:insert( self.left )
		physics.addBody( self.left, "static", { friction=0.0 } )

		self.right = display.newRect(circle.x + (width/2), circle.y, 1, height )
		group:insert( self.right )
		physics.addBody( self.right, "static", { friction=0.0 } )
	end
	--hole:setupBodies()

	hole.tap = onHoleTouch

	local text = display.newText( numShells, x, y + height/2, native.systemFont, 16 )
	hole.text = text
	if id:sub(1,1) == "B" then
		text:rotate(180)
	end
	group:insert( text )

	if false then
		local label = display.newText( id, x, y + height, native.systemFont, 16 )
		label:setFillColor(0, 0, 1)
		group:insert( label )
	end

	hole.shells = {}
	hole.addShell = function (self)
		local radius = 9
		local x_pos = x + math.random(-10,10)

		local whichImage = math.random(1,2)
		local whichRotation = math.random(0,360)
		--local shellDisplay = display.newCircle( x_pos, y + 1, radius ) 
		--shellDisplay:setFillColor(0,-23,-34)
		local shellDisplay = display.newImage("sigay" .. whichImage .. ".png")
		shellDisplay.x = x_pos
		shellDisplay.y = y + 1
		shellDisplay:rotate(whichRotation)
		group:insert(shellDisplay)
		self.shells[#self.shells+1] = shellDisplay
		physics.addBody( shellDisplay, { density = 1.0, friction = 0, bounce = 0, radius = radius  } )
	end

	hole.setNumShells = function (self, count)
		self.numShells = count
		self.text.text = count
		if count > #self.shells then
			for i = 1, count - #self.shells, 1 do
				self:addShell()
			end
		else
			for i = 1, #self.shells - count, 1 do
				local shell = self.shells[#self.shells]
				shell:removeSelf()
				table.remove(self.shells)
			end	
		end
	end

	hole:setNumShells(numShells)

	hole.setSunog = function (self, val)
		self.isSunog = val
		if self.isSunog then
			text:setFillColor(1, 1, 1)
		else
			text:setFillColor(1, 0, 0)
		end
	end
	hole:setSunog(false)

	hole.highlight = function (self, curPlayer)
		if curPlayer == "A" then
			if self.fill == fillB then
				self.fill = fillBoth
			else
				self.fill = fillA
			end
		else
			if self.fill == fillA then
				self.fill = fillBoth
			else
				self.fill = fillB
			end
		end
		self.circle:setFillColor(self.fill[1], self.fill[2], self.fill[3])
		self.isHighlighted = true
	end

	hole.fill = noFill
	hole.unHighlight = function (self, player)
		if self.fill == fillBoth and player ~= nil then
			if player == "A" then
				self.fill = fillB
			else
				self.fill = fillA
			end
		else
			self.fill = noFill
			self.isHighlighted = false
		end
		self.circle:setFillColor(self.fill[1], self.fill[2], self.fill[3])
	end
	hole:unHighlight()
	
	hole.removeBodies = function (self)
		if self.top ~= nil then
			self.top:removeSelf()
			self.bottom:removeSelf()
			self.left:removeSelf()
			self.right:removeSelf()
		end
		hole:setNumShells(0)
	end
	--print("created hole " .. hole.id)

	return hole
end

function resetGame(path)
	print("Starting " .. numPlayers .. " player game.")
	isQuitting = false
	for i=1, #path, 1 do
		local hole = path[i]
		if hole.id:sub(2,2) == "0" then
			hole:setNumShells(0)
		else
			hole:setNumShells(numShells)
		end
		hole:unHighlight()
	end
	curPlayer = "B"
	if numPlayers == 1 then
		switchTurns(path)
	else
		simultaneousTurns(path)
	end
end

function createGameScreen(group, numPlayers)
	-- create a grey rectangle as the backdrop
	local background = display.newRect(0, 0, screenW, screenH)
	background.anchorX = 0
	background.anchorY = 0
	background:setFillColor( .5 )
	group:insert( background )

	-- set up message area
	messageA = display.newText("", screenW/2, screenH - 15, native.systemFont, 16)
	messageA:setFillColor(0, 0, 0)
	group:insert( messageA )
	messageB = display.newText("", screenW/2, 15, native.systemFont, 16)
	messageB:setFillColor(0, 0, 0)
	if numPlayers == 2 then
		messageB:rotate(180)
	end
	group:insert( messageB )

	local btnExit = display.newText("X", screenW, 10, native.systemFont, 26)
	btnExit:addEventListener( "tap", btnExit )
	btnExit.tap = function ()
		print("quitting game")
		isQuitting = true
		for id, value in pairs(timer._runlist) do
     		timer.cancel(value)
		end
		storyboard.gotoScene( "menu", "crossFade", 1000 )
	end
	group:insert( btnExit )

	local autoText = display.newText("auto", screenW - 30, screenH - 65, native.systemFont, 16)
	group:insert( autoText )
	autoSwitch = widget.newSwitch({left = screenW - 50, top = screenH - 50, initialSwitchState=true})
	autoSwitch:rotate(270)
	group:insert( autoSwitch )

	-- set up game area
	local gameWidth = screenW
	local gameHeight = screenH * 0.9
	local gameLeft = screenW * 0.05
	local gameTop = screenH * 0.05
	local sectionWidth = gameWidth / (numBahay + 2)
	local width = (sectionWidth * 0.7)
	local height = 130
	local spacing = 5

	realPath = {}
	local pathNdx = 1
	for i = numBahay, 1, -1 do
		realPath[pathNdx] = makeHole("A"..i, group, pathNdx, gameLeft + (i*sectionWidth), halfH + spacing, width, height, numShells)
		pathNdx = pathNdx + 1
	end
	realPath[pathNdx] = makeHole("A0", group, pathNdx, gameLeft, halfH - height*1.5/2, width * 1.5, height * 1.5, 0)
	pathNdx = pathNdx + 1
	for i = numBahay, 1, -1 do
		realPath[pathNdx] = makeHole("B"..i, group, pathNdx, gameLeft + ((numBahay-i+1)*sectionWidth), halfH - height - spacing, width, height, numShells)
		pathNdx = pathNdx + 1
	end
	realPath[pathNdx] = makeHole("B0", group, pathNdx, gameLeft + gameWidth - sectionWidth, halfH - height*1.5/2, width * 1.5, height * 1.5, 0)
	
	-- prepare a table for quick id lookup of holes
	realPath.lookup = {}
	for i=1, #realPath, 1 do
		local hole = realPath[i]
		realPath.lookup[hole.id] = hole
	end

	--for i = 1, #path, 1 do
	--	print("path"..path[i].id)
	--end
end

-- Called when the scene's view does not exist:
function scene:createScene(event)
	local group = self.view
	numPlayers = event.params.numPlayers
	createGameScreen(group, numPlayers)
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view

	numPlayers = event.params.numPlayers
	physics.start()
	for i = 1, #realPath, 1 do
		realPath[i]:setupBodies()
	end
	resetGame(realPath)	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view

	for i = 1, #realPath, 1 do
		realPath[i]:removeBodies()
	end
	physics.stop()	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view

	package.loaded[physics] = nil
	physics = nil	
end

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

return scene