-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local widget = require("widget")
local gameNetwork = require( "gameNetwork" )

-- ads -- 
display.setStatusBar( display.HiddenStatusBar )
local ads = require( "ads" )

local statusText = display.newText( "", 0, 0, native.systemFontBold, 22 )
statusText:setFillColor( 255 )
statusText.x, statusText.y = display.contentWidth * 0.5, 160
statusText.x = display.contentWidth * 0.5

local showAd

local function adListener( event )
    local msg = event.response
    print("Message received from the ads library: ", msg)

    if event.isError then
        statusText:setTextColor( 255, 0, 0 )
        statusText.text = "Error Loading Ad"
        showAd( "banner" )
	else
        --statusText:setTextColor( 0, 255, 0 )
        --statusText.text = "Successfully Loaded Ad"
    end
end

ads.init("admob", "ca-app-pub-1625546980423526/5820163018", adListener)

local function showLeaderboards( event )
   if ( system.getInfo("platformName") == "Android" ) then
      gameNetwork.show( "leaderboards" )
   else
      gameNetwork.show( "leaderboards", { leaderboard = {timeScope="AllTime"} } )
   end
   return true
end


local btnOnePlayer
local btnTwoPlayers

local function onBtnOnePlayerRelease()
	local options = 
	{
		effect = "fade",
		time = 500,
		params =
		{
			numPlayers = 1,
		}
	}
	storyboard.gotoScene( "game", options )
	return true
end

local function onBtnTwoPlayersRelease()
	local options = 
	{
		effect = "fade",
		time = 500,
		params =
		{
			numPlayers = 2,
		}
	}
	storyboard.gotoScene( "game", options )
	return true
end

local function onBtnSettingsRelease()
	-- local options = 
	-- {
	-- 	effect = "fade",
	-- 	time = 500
	-- }
	-- storyboard.gotoScene( "settings", options )

	local myScore = 100

	--for GameCenter, default to the leaderboard name from iTunes Connect
	local myCategory = "com.gmail.ariscris.Sungka.highscores"

	if ( system.getInfo( "platformName" ) == "Android" ) then
	   --for GPGS, reset "myCategory" to the string provided from the leaderboard setup in Google
	   myCategory = "CgkIl8CcqbQCEAIQBw"
	end

	gameNetwork.request( "setHighScore",
	{
	   localPlayerScore = { category=myCategory, value=tonumber(myScore) },
	   listener = postScoreSubmit
	} )
end

local function showLeaderboards( event )

   return true
end

local function onBtnAchievementsRelease()
   gameNetwork.show( "achievements" )
   
   if ( system.getInfo("platformName") == "Android" ) then
      gameNetwork.show( "leaderboards" )
   else
      gameNetwork.show( "leaderboards", { leaderboard = {timeScope="AllTime"} } )
   end
end

local function postScoreSubmit( event )
   --whatever code you need following a score submission...
   return true
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view

	local background  = display.newRect(0, 0, display.contentWidth, display.contentHeight)
	--background:setFillColor(255)
	background.x, background.y = display.contentWidth / 2, display.contentHeight / 2

	
	-- create/position logo/title image on upper-half of the screen
	local titleLogo = display.newImageRect( "logo.png", 264, 42 )
	titleLogo.x = display.contentWidth * 0.5
	titleLogo.y = 100
	
	-- create a widget button (which will loads level1.lua on release)
	btnOnePlayer = widget.newButton{
		label="One Player",
		labelColor = { default={0}, over={128} },
		default="button.png",
		over="button-over.png",
		width=154, height=40,
		onRelease = onBtnOnePlayerRelease
	}
	btnOnePlayer.x = display.contentWidth*0.5
	btnOnePlayer.y = display.contentHeight - 125
	
	btnTwoPlayers = widget.newButton{
		label="Two Players",
		labelColor = { default={0}, over={128} },
		default="button.png",
		over="button-over.png",
		width=154, height=40,
		onRelease = onBtnTwoPlayersRelease
	}
	btnTwoPlayers.x = display.contentWidth*0.5
	btnTwoPlayers.y = display.contentHeight - 95

	btnSettings = widget.newButton{
		label="settings",
		labelColor = { default={0}, over={128} },
		default="button.png",
		over="button-over.png",
		width=154, height=40,
		onRelease = onBtnSettingsRelease
	}
	btnSettings.x = display.contentWidth*0.5
	btnSettings.y = display.contentHeight - 45

	btnAchievements = widget.newButton{
		label="achievements",
		labelColor = { default={0}, over={128} },
		default="button.png",
		over="button-over.png",
		width=154, height=40,
		onRelease = onBtnAchievementsRelease
	}
	btnAchievements.x = display.contentWidth*0.5
	btnAchievements.y = display.contentHeight - 15

	-- all display objects must be inserted into group
	group:insert( background )
	group:insert( titleLogo )
	group:insert( btnOnePlayer )
	group:insert( btnTwoPlayers )
	group:insert( btnSettings )
	group:insert( btnAchievements )

end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view

	statusText:toFront()
	group:insert( statusText )

	local adX = 0
	local adY = display.contentHeight - 50
	showAd = function( adType )
        statusText.text = ""
        ads.show( adType, { x=adX, y=adY } )
	end

	-- if on simulator, let user know they must build for device
	if system.getInfo("environment") == "simulator" then
        local font, size = native.systemFontBold, 12
        local warningText = display.newText( "Please build for device or Xcode simulator to test ads.", 0, 0, 350, 300, font, size )
        warningText:setFillColor( 255 )
        warningText.x, warningText.y = adX, adY
		group:insert( warningText )
	else
        -- start with banner ad
        showAd( "interstitial" )
	end
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	ads.hide()
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	if playBtn then
		playBtn:removeSelf()	-- widgets must be manually removed
		playBtn = nil
	end
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene