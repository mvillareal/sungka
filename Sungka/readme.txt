Signing

ariss-mbp:sungka ariscris$ keytool -genkey -v -alias androidreleasekey -keystore /Applications/CoronaSDK/Resource\ Library/Android/release.keystore -keyalg RSA -keysize 2048 -validity 10000
Enter keystore password:  
What is your first and last name?
  [Unknown]:  Maricris Villareal
What is the name of your organizational unit?
  [Unknown]:  
What is the name of your organization?
  [Unknown]:  
What is the name of your City or Locality?
  [Unknown]:  Clifton
What is the name of your State or Province?
  [Unknown]:  NJ
What is the two-letter country code for this unit?
  [Unknown]:  US
Is CN=Maricris Villareal, OU=Unknown, O=Unknown, L=Clifton, ST=NJ, C=US correct?
  [no]:  yes

Generating 2,048 bit RSA key pair and self-signed certificate (SHA1withRSA) with a validity of 10,000 days
	for: CN=Maricris Villareal, OU=Unknown, O=Unknown, L=Clifton, ST=NJ, C=US
Enter key password for <androidreleasekey>
	(RETURN if same as keystore password):  
[Storing /Applications/CoronaSDK/Resource Library/Android/release.keystore]

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore /Applications/CoronaSDK/Resource\ Library/Android/release.keystore Sungka.apk androidreleasekey

zipalign -v 4 Sungka.apk SungkaAligned.apk